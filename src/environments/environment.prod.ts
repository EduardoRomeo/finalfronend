export const environment = {
  production: true,
  api: {
    host: 'https://reqres.in',
  }
};
