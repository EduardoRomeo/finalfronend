import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthenticationService {

  private readonly PASSPHRASE = 'angular-dh';
  private readonly KEY = 'token';

  constructor() {
  }

  public setToken(token: string): void {
    localStorage.setItem(this.KEY, CryptoJS.AES.encrypt(token, this.PASSPHRASE));
  }

  public getToken(): string {
    let response: string;

    const encryptedToken = localStorage.getItem(this.KEY);

    if (encryptedToken) {
      const bytes = CryptoJS.AES.decrypt(encryptedToken, this.PASSPHRASE);
      response = bytes.toString(CryptoJS.enc.Utf8);
    }

    return response;
  }

  public removeToken(): void {
    localStorage.removeItem(this.KEY);
  }
}
