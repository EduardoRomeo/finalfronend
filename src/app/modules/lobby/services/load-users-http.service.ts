import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../../bootstrap/http.service';
import {Observable} from 'rxjs';

@Injectable()
export class LoadUsersHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/users';
  }

  public doGet(page: number, size: number): Observable<object> {
    return this.httpClient().get(`${this.getUrl()}?page=${page}&per_page=${size}`);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
