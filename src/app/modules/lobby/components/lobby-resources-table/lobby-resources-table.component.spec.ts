import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyResourcesTableComponent } from './lobby-resources-table.component';

describe('LobbyResourcesComponent', () => {
  let component: LobbyResourcesTableComponent;
  let fixture: ComponentFixture<LobbyResourcesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyResourcesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyResourcesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
