import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lobby-resources',
  templateUrl: './lobby-resources-table.component.html',
  styleUrls: ['./lobby-resources-table.component.scss']
})
export class LobbyResourcesTableComponent implements OnInit {

  public countries: Country[];

  constructor() {
    this.countries = COUNTRIES;
  }

  ngOnInit() {
  }


}

interface Country {
  name: string;
  flag: string;
  area: number;
  population: number;
}

const COUNTRIES: Country[] = [
  {
    name: 'Bolivia',
    flag: '4/48/Flag_of_Bolivia.svg',
    area: 1098581,
    population: 11383094
  },
  {
    name: 'Argentina',
    flag: '1/1a/Flag_of_Argentina.svg',
    area: 17075200,
    population: 146989754
  },
  {
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199
  },
  {
    name: 'Brazil',
    flag: '0/05/Flag_of_Brazil.svg',
    area: 9629091,
    population: 324459463
  },
  {
    name: 'Uruguay',
    flag: 'f/fe/Flag_of_Uruguay.svg',
    area: 9596960,
    population: 1409517397
  },
  {
    name: 'Russia',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: 17075200,
    population: 146989754
  },
  {
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199
  },
  {
    name: 'United States',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: 9629091,
    population: 324459463
  },
  {
    name: 'China',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: 9596960,
    population: 1409517397
  }
];
