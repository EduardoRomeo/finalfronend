import {Component, Input} from '@angular/core';
import {LobbyUser} from '../lobby-users/lobby-user';

@Component({
  selector: 'app-lobby-user-item',
  templateUrl: './lobby-user-item.component.html',
  styleUrls: ['./lobby-user-item.component.scss']
})
export class LobbyUserItemComponent {

  @Input() public user: LobbyUser;
  @Input() public index: number;

  private readonly EMPTY = '';

  constructor() {
    this.user = {
      avatar: this.EMPTY,
      email: this.EMPTY,
      last_name: this.EMPTY,
      first_name: this.EMPTY,
      id: this.EMPTY
    };

    this.index = 0;
  }

}
