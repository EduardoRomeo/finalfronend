import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyCustomSecondComponent } from './lobby-custom-second.component';

describe('LobbyCustomSecondComponent', () => {
  let component: LobbyCustomSecondComponent;
  let fixture: ComponentFixture<LobbyCustomSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyCustomSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyCustomSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
