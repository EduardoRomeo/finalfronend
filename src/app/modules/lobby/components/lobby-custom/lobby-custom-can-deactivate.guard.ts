import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {LobbyCustomDetailsComponent} from './lobby-custom-details/lobby-custom-details.component';

@Injectable({
  providedIn: 'root'
})
export class LobbyCustomCanDeactivateGuard implements CanDeactivate<LobbyCustomDetailsComponent> {

  canDeactivate(component: LobbyCustomDetailsComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    console.log('Entrando al guard CanDeactivate');

    // Validar por ejemplo si un formulario tiene datos que no han sido guardados
    if (this.hasDataNotSaved()) {
      alert('por favor guarde la informacion antes de proceder');
      return false;
    }

    return true;
  }

  public hasDataNotSaved(): boolean {
    /*return Math.random() >= 0.5;*/
    return false;
  }

}
