import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyCustomMainComponent } from './lobby-custom-main.component';

describe('LobbyCustomMainComponent', () => {
  let component: LobbyCustomMainComponent;
  let fixture: ComponentFixture<LobbyCustomMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyCustomMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyCustomMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
