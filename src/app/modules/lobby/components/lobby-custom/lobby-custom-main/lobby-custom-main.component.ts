import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-lobby-custom-main',
  templateUrl: './lobby-custom-main.component.html',
  styleUrls: ['./lobby-custom-main.component.scss']
})
export class LobbyCustomMainComponent implements OnInit {

  public title: string;

  constructor(private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.data.subscribe((data: { title: string, second: string }) => {
      if (data) {
        this.title = data.title;
      }
    });
  }
}
