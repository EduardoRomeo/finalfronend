import { TestBed, async, inject } from '@angular/core/testing';

import { LobbyCustomGuard } from './lobby-custom.guard';

describe('LobbyCustomGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LobbyCustomGuard]
    });
  });

  it('should ...', inject([LobbyCustomGuard], (guard: LobbyCustomGuard) => {
    expect(guard).toBeTruthy();
  }));
});
