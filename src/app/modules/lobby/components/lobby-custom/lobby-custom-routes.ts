import {Routes} from '@angular/router';
import {LobbyCustomMainComponent} from './lobby-custom-main/lobby-custom-main.component';
import {LobbyCustomFirstComponent} from './lobby-custom-first/lobby-custom-first.component';
import {LobbyCustomSecondComponent} from './lobby-custom-second/lobby-custom-second.component';
import {LobbyCustomDetailsComponent} from './lobby-custom-details/lobby-custom-details.component';
import {LobbyCustomGuard} from './lobby-custom.guard';
import {LobbyCustomCanDeactivateGuard} from './lobby-custom-can-deactivate.guard';

export const CUSTOM_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: LobbyCustomMainComponent,
    canActivate: [LobbyCustomGuard],
    data: {title: 'Another title!', second: 'asdfasfd'},
    children: [
      {
        path: 'first',
        component: LobbyCustomFirstComponent,
        canActivateChild: [LobbyCustomGuard],
        children: [
          {
            path: ':id',
            component: LobbyCustomDetailsComponent,
            canDeactivate: [LobbyCustomCanDeactivateGuard]
          }
        ]
      },
      {
        path: 'second',
        component: LobbyCustomSecondComponent
      }
    ]
  }
];
