import {NgModule} from '@angular/core';

import {LobbyCustomRoutingModule} from './lobby-custom-routing.module';
import {LobbyCustomFirstComponent} from "./lobby-custom-first/lobby-custom-first.component";
import {LobbyCustomDetailsComponent} from "./lobby-custom-details/lobby-custom-details.component";
import {LobbyCustomMainComponent} from "./lobby-custom-main/lobby-custom-main.component";
import {LobbyCustomSecondComponent} from "./lobby-custom-second/lobby-custom-second.component";
import {SharedModule} from "../../../shared/shared.module";


@NgModule({
  declarations: [LobbyCustomFirstComponent,
    LobbyCustomSecondComponent,
    LobbyCustomMainComponent,
    LobbyCustomDetailsComponent],
  imports: [
    SharedModule,
    LobbyCustomRoutingModule
  ]
})
export class LobbyCustomModule {
}
