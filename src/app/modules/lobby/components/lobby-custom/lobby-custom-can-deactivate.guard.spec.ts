import { TestBed, async, inject } from '@angular/core/testing';

import { LobbyCustomCanDeactivateGuard } from './lobby-custom-can-deactivate.guard';

describe('LobbyCustomCanDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LobbyCustomCanDeactivateGuard]
    });
  });

  it('should ...', inject([LobbyCustomCanDeactivateGuard], (guard: LobbyCustomCanDeactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
