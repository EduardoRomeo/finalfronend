import {Component, OnInit} from '@angular/core';
import {LobbyCustomFirstService} from './lobby-custom-first.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-lobby-custom-first',
  templateUrl: './lobby-custom-first.component.html',
  styleUrls: ['./lobby-custom-first.component.scss']
})
export class LobbyCustomFirstComponent implements OnInit {

  public products: { id: number, name: string, description: string }[];

  constructor(private _lobbycustomFirstService: LobbyCustomFirstService,
              private _router: Router) {
    this.products = [];
  }

  ngOnInit() {
    this.products = this._lobbycustomFirstService.products;
  }

  public navigate(id: number): void {
    this._router.navigate(['custom', 'first', +id]).then
    ((value: boolean) => {
      // console.log(value);
    });
  }
}
