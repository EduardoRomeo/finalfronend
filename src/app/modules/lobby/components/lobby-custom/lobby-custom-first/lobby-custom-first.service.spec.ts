import { TestBed } from '@angular/core/testing';

import { LobbyCustomFirstService } from './lobby-custom-first.service';

describe('LobbyCustomFirstService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LobbyCustomFirstService = TestBed.get(LobbyCustomFirstService);
    expect(service).toBeTruthy();
  });
});
