import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LobbyCustomFirstService {

  private _products: { id: number, name: string, description: string }[];

  constructor() {
    this._products = [
      {
        id: 1,
        name: 'Leche',
        description: 'Producto líquido alimenticio.'
      },
      {
        id: 2,
        name: 'Pan',
        description: 'Producto compuesto de almidón'
      },
      {
        id: 3,
        name: 'Duraznos al jugo',
        description: 'Producto enlatado procesado'
      },
      {
        id: 4,
        name: 'Coca Cola',
        description: 'Producto procesado.'
      }
    ];
  }

  public getById(id: number): { id: number, name: string, description: string } {
    let response: { id: number, name: string, description: string } = null;

    for (let product of this._products) {
      if (product.id === id) {
        response = product;
      }
    }

    return response;
  }

  get products(): { id: number, name: string, description: string }[] {
    return this._products;
  }
}
