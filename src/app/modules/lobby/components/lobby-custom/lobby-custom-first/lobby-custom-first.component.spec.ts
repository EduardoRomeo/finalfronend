import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyCustomFirstComponent } from './lobby-custom-first.component';

describe('LobbyCustomFirstComponent', () => {
  let component: LobbyCustomFirstComponent;
  let fixture: ComponentFixture<LobbyCustomFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyCustomFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyCustomFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
