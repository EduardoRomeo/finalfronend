import {Component, OnInit} from '@angular/core';
import {LobbyCustomFirstService} from '../lobby-custom-first/lobby-custom-first.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-lobby-details',
  templateUrl: './lobby-custom-details.component.html',
  styleUrls: ['./lobby-custom-details.component.scss']
})
export class LobbyCustomDetailsComponent implements OnInit {

  public product: { id: number, name: string, description: string };

  constructor(private _lobbycustomFirstService: LobbyCustomFirstService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    /*let id = this._activatedRoute.snapshot.paramMap.get('id');
    this.product = this._customFirstService.getById(+id);*/

    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.product = this._lobbycustomFirstService.getById(+id);
    });
  }

}
