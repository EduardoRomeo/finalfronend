import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyCustomDetailsComponent } from './lobby-custom-details.component';

describe('LobbyCustomDetailsComponent', () => {
  let component: LobbyCustomDetailsComponent;
  let fixture: ComponentFixture<LobbyCustomDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyCustomDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyCustomDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
