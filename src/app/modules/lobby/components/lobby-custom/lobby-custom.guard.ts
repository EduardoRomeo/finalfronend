import {Injectable} from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanActivateChild, CanLoad, UrlSegment, Route
} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LobbyCustomGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private _router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('Entrando al guard CanActivate');

    /*Lógica de vista simulada*/
    let isLoggedIn = this.isLoggedIn();
    if (!isLoggedIn) {
      this._router.navigate(['unknown']);
      return false;
    }

    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('Entrando al guard canActivateChild');
    return this.canActivate(childRoute, state);
  }

  public isLoggedIn(): boolean {
    /*return Math.random() >= 0.5;*/
    return true;
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    console.log('Entrando al guard canLoad');
    return true;
  }
}
