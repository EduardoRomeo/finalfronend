import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CUSTOM_ROUTES_CONFIG} from './lobby-custom-routes';

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(CUSTOM_ROUTES_CONFIG)
  ],
  exports: [
    RouterModule
  ]
})
export class LobbyCustomRoutingModule {
}
