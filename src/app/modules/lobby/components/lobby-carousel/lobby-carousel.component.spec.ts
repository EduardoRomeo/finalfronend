import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyCarouselComponent } from './lobby-carousel.component';

describe('LobbyCarouselComponent', () => {
  let component: LobbyCarouselComponent;
  let fixture: ComponentFixture<LobbyCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
