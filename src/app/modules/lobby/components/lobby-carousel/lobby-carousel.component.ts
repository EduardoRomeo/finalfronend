import {Component, OnInit} from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-lobby-carousel',
  templateUrl: './lobby-carousel.component.html',
  styleUrls: ['./lobby-carousel.component.scss']
})
export class LobbyCarouselComponent implements OnInit {

  public images = [1, 2, 3, 4, 5].map(() =>
    `https://picsum.photos/1200/500?random&t=${Math.random()}`);

  constructor(private config: NgbCarouselConfig) {
    config.interval = 5000;
    config.wrap = false;
    config.keyboard = true;
    config.pauseOnHover = false;
  }

  ngOnInit() {
  }

}
