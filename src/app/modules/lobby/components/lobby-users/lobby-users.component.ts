import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoadUsersHttpService} from '../../services/load-users-http.service';
import {LobbyUser} from './lobby-user';
import {LobbyUsersModel} from '../models/lobby-users.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-lobby-users',
  templateUrl: './lobby-users.component.html',
  styleUrls: ['./lobby-users.component.scss']
})
export class LobbyUsersComponent implements OnInit, OnDestroy {

  public users: LobbyUser[];

  public readonly size = 3;

  private lobbyUserModelSubscription: Subscription;
  private loadUsersSubscription: Subscription;

  constructor(private loadUsersHttpService: LoadUsersHttpService,
              private lobbyUsersModel: LobbyUsersModel) {
  }

  ngOnInit() {
    this.loadUsers(this.lobbyUsersModel.page, this.size);

    this.lobbyUserModelSubscription = this.lobbyUsersModel.asObservable().subscribe(
      (users: LobbyUser[]) => {
        this.users = users;
      }
    );
  }

  ngOnDestroy(): void {
    this.lobbyUsersModel.init();

    this._unsubscribe(this.loadUsersSubscription);
    this._unsubscribe(this.lobbyUserModelSubscription);
  }

  public loadUsers(page: number, size = this.size): void {
    this.loadUsersSubscription = this.loadUsersHttpService.doGet(page, size).subscribe(
      (response: any) => {
        this.lobbyUsersModel.page++;

        const users = response.data;
        this.lobbyUsersModel.addUsers(users);
      }
    );
  }

  public onScroll(): void {
    this.loadUsers(this.lobbyUsersModel.page);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
