import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {LobbyUser} from '../lobby-users/lobby-user';

@Injectable()
export class LobbyUsersModel extends BehaviorSubject<LobbyUser[]> {

  public users: LobbyUser[];

  private _page: number;

  constructor() {
    super([]);
    this.users = [];

    this._page = 1;
  }

  public init(): void {
    this.users = [];
    this._page = 1;

    this.next([]);
  }

  public addUsers(users: LobbyUser[]): void {
    for (const user of users) {
      const found = this.users.find(
        (userStored: LobbyUser) => {
          return userStored.id === user.id;
        }
      );

      if (!found) {
        this._pushUser(user);
      }

    }
    this.next(this.users);
  }

  private _pushUser(user: LobbyUser): void {
    this.users.push(user);
  }

  public get page(): number {
    return this._page;
  }

  public set page(page: number) {
    this._page = page;
  }
}
