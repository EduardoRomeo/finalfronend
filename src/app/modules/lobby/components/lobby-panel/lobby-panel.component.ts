import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-lobby-panel',
  templateUrl: './lobby-panel.component.html',
  styleUrls: ['./lobby-panel.component.scss']
})
export class LobbyPanelComponent implements OnInit {

  public index: number;

  constructor() {
    this.index = -1;
  }

  ngOnInit() {
  }

  public openNext(): void {
    this.index = (this.index === 3) ? 0 : this.index + 1;
  }

  public openPrev(): void {
    this.index = (this.index <= 0) ? 3 : this.index - 1;
  }

}
