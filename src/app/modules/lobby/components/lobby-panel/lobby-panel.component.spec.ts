import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyPanelComponent } from './lobby-panel.component';

describe('LobbyPanelComponent', () => {
  let component: LobbyPanelComponent;
  let fixture: ComponentFixture<LobbyPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
