import {Routes} from '@angular/router';
import {LobbyMainComponent} from '../components/lobby-main/lobby-main.component';
import {LobbyUsersComponent} from '../components/lobby-users/lobby-users.component';
import {LobbyResourcesTableComponent} from '../components/lobby-resources-table/lobby-resources-table.component';
import {LobbyCarouselComponent} from '../components/lobby-carousel/lobby-carousel.component';
import {LobbyPanelComponent} from "../components/lobby-panel/lobby-panel.component";
import {TodoListComponent} from "../components/todo/todo-list/todo-list.component";
import {TodoFormComponent} from "../components/todo/todo-form/todo-form.component";


export const LOBBY_ROUTES: Routes = [
  {
    path: '',
    component: LobbyMainComponent,
    children: [
      {
        path: 'users',
        component: LobbyUsersComponent
      },
      {
        path: 'resources',
        component: LobbyResourcesTableComponent
      },
      {
        path: 'carousel',
        component: LobbyCarouselComponent
      },
      {
        path: 'panel',
        component: LobbyPanelComponent
      },
      {
        path: 'form',
        component: TodoFormComponent
      },
      {
        path: 'list',
        component: TodoListComponent
      },
      {
        path: '',
        redirectTo: '/lobby/users',
        pathMatch: 'full'
      }
    ]
  }
];
