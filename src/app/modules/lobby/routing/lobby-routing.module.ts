import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {LOBBY_ROUTES} from './lobby.routes';

@NgModule({
  imports: [RouterModule.forChild(LOBBY_ROUTES)],
  exports: [RouterModule]
})
export class LobbyRoutingModule {
}
