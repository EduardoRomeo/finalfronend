import {NgModule} from '@angular/core';
import {LobbyMainComponent} from './components/lobby-main/lobby-main.component';
import {LobbyRoutingModule} from './routing/lobby-routing.module';
import {SharedModule} from '../shared/shared.module';
import {LobbyUsersComponent} from './components/lobby-users/lobby-users.component';
import {LobbyNavComponent} from './components/lobby-nav/lobby-nav.component';
import {LoadUsersHttpService} from './services/load-users-http.service';
import {LobbyUsersModel} from './components/models/lobby-users.model';
import {LobbyUserItemComponent} from './components/lobby-user-item/lobby-user-item.component';
import {LobbyResourcesTableComponent} from "./components/lobby-resources-table/lobby-resources-table.component";

import {NgbCarouselModule} from "@ng-bootstrap/ng-bootstrap";
import {LobbyCarouselComponent} from "./components/lobby-carousel/lobby-carousel.component";
import {LobbyPanelComponent} from "./components/lobby-panel/lobby-panel.component";
import {AccordionModule} from "primeng/accordion";
import {CardModule} from "primeng/card";
import {TodoFormComponent} from "./components/todo/todo-form/todo-form.component";
import {TodoListComponent} from "./components/todo/todo-list/todo-list.component";

@NgModule({
  declarations: [LobbyMainComponent,
    LobbyUsersComponent,
    LobbyNavComponent,
    LobbyUserItemComponent,
    LobbyResourcesTableComponent,
    LobbyCarouselComponent,
    LobbyPanelComponent,
    TodoFormComponent,
    TodoListComponent
  ],
  imports: [
    SharedModule,
    LobbyRoutingModule,
    NgbCarouselModule,
    AccordionModule,
    CardModule,
  ],
  providers: [
    /*Http Services*/
    LoadUsersHttpService,
    /*Models*/
    LobbyUsersModel
  ]
})
export class LobbyModule {
}
