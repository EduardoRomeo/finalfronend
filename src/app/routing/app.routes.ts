import {Routes} from '@angular/router';
import {NotFoundComponent} from '../components/not-found/not-found.component';
import {AuthGuard} from './guards/auth.guard';

export const APP_ROUTES: Routes = [
  {
    path: 'secure',
    loadChildren: '../modules/secure/secure.module#SecureModule',
  },
  {
    path: 'lobby',
    loadChildren: '../modules/lobby/lobby.module#LobbyModule',
    canLoad: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/secure/login',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
